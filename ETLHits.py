import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import copy
import mpl_toolkits.mplot3d.art3d as art3d

### Dimensions of LGADs ###
L = 20.8 #Length of both sides in mm
NumCells = 16 #Number of cells on side
d12 = L/2  #Distance between LGAD layers 1 & 2
d23 = L/2 #Distance between LGAD layers 2 & 3
mu = 0   #Mean of deviation of hit from layer 1 to layer 2
sigma = L/10 #std dev of deviation of hits from layer 1 to layer 2

def GenerateTracks(N):
    TrueHits1 = np.array([[L*np.random.rand(), L*np.random.rand(), 0] for i in range(N)])
    TrueHits2 = np.array([[TrueHits1[i][0] + np.random.normal(mu, sigma), TrueHits1[i][1] + np.random.normal(mu, sigma), -d12] for i in range(N)])
    
    '''
    TrueHits2 takes the coordinates of the hits from TrueHits1 and adds a deviation taken from a normal disribution with mean == mu and stddev = sigma
    '''
    
    TrueHits3 = (d23/d12)*(TrueHits2 - TrueHits1) + TrueHits2
    TrueTracks = [[TrueHits1[i], TrueHits2[i], TrueHits3[i]] for i in range(len(TrueHits1))]
    
    '''
    TrueTracks groups hits into tracks as such: 
    TrueTrack[0] = [[coordinates of hit in layer 1], [coordinates of hit in layer 2], [coordinates of hit in layer 3]]
    '''

    return [TrueHits1, TrueHits2, TrueHits3, TrueTracks]

def IsColinear(TruthTuple):
    determinants = []
    matrices = [[TruthTuple[0][i], TruthTuple[1][i], TruthTuple[2][i]] for i in range(3)]
    for matrix in matrices:
        determinants.append(np.linalg.det(matrix))
    return determinants

def DiscretizeHits(TruthTuple):
    DiscreteHits = copy.deepcopy(TruthTuple)
    for i in range(3):
        for hit in DiscreteHits[i]:
            hit[0] = hit[0] // (L/NumCells)
            hit[1] = hit[1] // (L/NumCells)
    return DiscreteHits

def ReconstructTracks(DiscreteHits):
    RecoTracks = []
    for hit1 in DiscreteHits[0]:
        for hit2 in DiscreteHits[1]:
            check_hit3 = (d23/d12)*(hit2 - hit1) + hit2
            for hit3 in DiscreteHits[2]:
                if hit3[0]-1 <= check_hit3[0] <= hit3[0]+1 and hit3[1]-1 <= check_hit3[1] <= hit3[1]+1:
                    RecoTracks.append([hit1, hit2, hit3])
    return RecoTracks

TruthTuple = GenerateTracks(1)

DiscreteHits = DiscretizeHits(TruthTuple)

RecoTracks = ReconstructTracks(DiscreteHits)

fig = plt.figure(dpi=1200)
TruthPlot = fig.add_subplot(1, 2, 1, projection='3d')
TruthPlot.title.set_text('Truth Tracks')

TruthPlot.axes.set_xlim3d(0, L)
TruthPlot.axes.set_ylim3d(0, L)
TruthPlot.axes.set_zlim3d(-d12-d23, 0)


for i, track in enumerate(TruthTuple[3]):
    farbe = ['red', 'blue', 'green', 'purple', 'orange', 'yellow', 'grey', 'pink']
    for hit in track:
        TruthPlot.scatter(hit[0], hit[1], hit[2], color=farbe[i])    
    TruthPlot.plot([track[0][0], track[2][0]], [track[0][1], track[2][1]], [track[0][2], track[2][2]], color=farbe[i])


RecoPlot = fig.add_subplot(1, 2, 2, projection='3d')
RecoPlot.title.set_text('Reco Tracks')

RecoPlot.axes.set_xlim3d(0, NumCells)
RecoPlot.axes.set_ylim3d(0, NumCells)
RecoPlot.axes.set_zlim3d(-d12-d23, 0)

for i, track in enumerate(RecoTracks):
    farbe = ['red', 'blue', 'green', 'purple', 'orange', 'yellow', 'grey', 'pink']
    for hit in track:
        square = Rectangle(hit[:2], 1, 1, color=farbe[i])
        RecoPlot.add_patch(square)
        art3d.pathpatch_2d_to_3d(square, z = hit[2], zdir = "z")
    #RecoPlot.plot([track[0][0], track[2][0]], [track[0][1], track[2][1]], [track[0][2], track[2][2]], color=farbe[i])

